## Purpose

This repo was built with the same premise as [arduino-experiments over at 
Github](https://github.com/OdinsHat/arduino-experiments). To have an open area 
where I learnt Arduino with increasingly difficult projects under different 
directories with explanations of what each does, how and why.

## Why?

I found this a REALLY useful way of solidifying in my own mind the lessons I 
learnt about Arduino and thought I'd had a few false starts with VueJS so 
thought I'd give it a go using this method for VueJS.

## How Complex Will You Go?

One proviso of thes projects is as they get more complex I reserve the right 
to spin them off into full blown sites independent of this repo and will keep 
the code in a seperate private repo. This repos directory for that "experiment"
will contain a README simply pointing to the site the experiment produced with 
a summary of what was done to create it.

## Questions?

If you happen to see this repo (its on Gitlab which has far less social 
interactivity) but still if you have any questions along the way feel free to submit
an issue and I'll do my best to answer.

## First Experiments

* [First method and data](https://jsfiddle.net/eywraw8t/123943/)
